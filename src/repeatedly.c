/*
 * Execute a command, repeatedly.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/wait.h>

#define ARG_NUMBER_OF_ITERATIONS "number-of-iterations"
#define ARG_CONTINUE_ON_ERROR "continue-on-error"
#define ARG_VERBOSE "verbose"
#define ARG_HELP "help"

static int continue_on_error = 0;
static int number_of_iterations = 0;
static int verbose = 0;

static struct option long_options[] = {
	{ ARG_NUMBER_OF_ITERATIONS, required_argument, NULL, 0 },
	{ ARG_CONTINUE_ON_ERROR, no_argument, NULL, 0 },
	{ ARG_VERBOSE, no_argument, NULL, 0 },
	{ ARG_HELP, no_argument, NULL, 0 },
	{ NULL, 0, NULL, 0 }
};

static void
show_usage(const char *progname)
{
	printf("Usage: %s [options] -- [program and args]\n", progname);
	printf("\nOptions:\n");
	printf("    -h --help                    Show help\n");
	printf("    -n --number-of-iterations    Number of times to run the program, or 0 to run forever (default: 0)\n");
	printf("    -c --continue-on-error       Continue running the program even if it returns an error\n");
	printf("    -v --verbose                 Print extra information to stderr\n");
}

static int
start_and_run(const char *prog, char *const args[])
{
	siginfo_t infop;

	pid_t pid = fork();

	switch(pid) {
	case -1: // Error
		perror("fork");
		exit(EXIT_FAILURE);
	case 0: // Child
		execvp(prog, args);
		break;
	default: // Parent
		if(waitid(P_PID, pid, &infop, WEXITED) == -1) {
			perror("waitid");
			exit(EXIT_FAILURE);
		}
		if(infop.si_status != EXIT_SUCCESS) {
			fprintf(stderr, "%s\n", strerror(infop.si_status));
			return EXIT_FAILURE;
		}
		break;
	}

	return EXIT_SUCCESS;
}

int
main(int argc, char **argv)
{
	int c;
	int option_index = 0;

	while(1) {
		c = getopt_long(argc, argv,
				"n:cvh?",
				long_options, &option_index);

		if(c == -1) {
			break;
		}
		
		switch(c) {
		case 0:
			if(strcmp(ARG_NUMBER_OF_ITERATIONS, long_options[option_index].name) == 0) {
				number_of_iterations = atoi(optarg);
				break;
			}
			if(strcmp(ARG_CONTINUE_ON_ERROR, long_options[option_index].name) == 0) {
				continue_on_error = 1;
			}
			break;
		case 'n':
			number_of_iterations = atoi(optarg);
			break;
		case 'c':
			continue_on_error = 1;
			break;
		case 'v':
			verbose = 1;
			break;
		case 'h':
		case '?':
			show_usage(argv[0]);
			return EXIT_SUCCESS;
		}
	}

	if(verbose) {
		fprintf(stderr, "Number of iterations = %d\n", number_of_iterations);
		fprintf(stderr, "Continue on error = %d\n", continue_on_error);
	}

	int prog_ind = optind;
	for(int i = 0; number_of_iterations == 0 || i < number_of_iterations; i++) {
		if(verbose) {
			fprintf(stderr, "Starting iteration %d\n", i+1);
		}
		int rc = start_and_run(argv[prog_ind], &argv[prog_ind]);
		if(verbose) {
			fprintf(stderr, "Exit code %d\n", rc);
		}
		if(rc != EXIT_SUCCESS && continue_on_error == 0) {
			return rc;
		}
	}
	
	return EXIT_SUCCESS;
}
