# repeatedly

A small C program to run a program repeatedly, either for a given number of iterations or until it returns a failure code.

# Getting started

This program has been tested on Linux, although it may compile and work on other UNIX variants. By default, `clang` is used to compile the source, but this can be switch to `gcc` by editing the `Makefile`.

Type "`make`". If successful, the `repeatedly` binary can be found in the `bin` directory.

`repeatedly -h` gives an overview of usage.

A manual page is provided as `man/repeatedly.1`