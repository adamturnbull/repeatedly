CC		= clang
CFLAGS		= -g -Wall

SRCDIR		= src
OBJDIR		= obj
BINDIR		= bin

.PHONY:		prepare clean

all:		prepare $(BINDIR)/repeatedly

prepare:
		if [ ! -d $(OBJDIR) ]; then mkdir -p $(OBJDIR); fi; \
		if [ ! -d $(BINDIR) ]; then mkdir -p $(BINDIR); fi

clean:
		rm -rf $(OBJDIR) $(BINDIR)

$(BINDIR)/repeatedly:	$(OBJDIR)/repeatedly.o
			$(CC) $(LDFLAGS) $< -o $@

$(OBJDIR)/repeatedly.o:	$(SRCDIR)/repeatedly.c
			$(CC) $(CFLAGS) -c $< -o $@

